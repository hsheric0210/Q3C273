# Quasar.Win32PE

ALL OF ITS ORIGINAL SOURCE AVAILABLE AT [stjeong/DotNetSamples](https://github.com/stjeong/DotNetSamples/)

* [WindowsPE](https://github.com/stjeong/DotNetSamples/tree/master/WinConsole/PEFormat/WindowsPE) -> `Win32PE/PE`
* [KernelStructOffset](https://github.com/stjeong/DotNetSamples/tree/master/WinConsole/Debugger/KernelStructOffset) -> `Win32PE/Structs`

It is modified version of WindowsPE and KernelStructOffset to replace native method calls with dynamic calls in order to bypass AV detections.
